package com.are_you_learn.dao;

import com.are_you_learn.beans.Academic;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CONG
 * @date 2019-10-09 19:51
 **/

@Mapper
public interface AcademicMapper {


    /**
     * 获取专业
     * @param academicId
     * @return
     */
    Academic getAcademicById(Integer academicId) ;
}
