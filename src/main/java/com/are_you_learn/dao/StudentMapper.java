package com.are_you_learn.dao;

import com.are_you_learn.beans.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author CONG
 * @date 2019-09-22 20:13
 **/
@Mapper
public interface StudentMapper {

    /**
     * 添加学生
     * @param student
     */
    void insertStudent(Student student);

    /**
     * 查找该学号是否存在
     * @param studentUsername
     */
    int findStudent(String studentUsername);

    /**
     * 按username查找学生
     * @param studentUsername
     * @return
     */
    Student selectStudentByUsername(String studentUsername);

    /**
     * 添加登陆记录
     * @param record
     */
    void addLoginRecord(Map<String, Object> record);
}
