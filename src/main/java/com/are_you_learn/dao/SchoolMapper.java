package com.are_you_learn.dao;

import com.are_you_learn.beans.School;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author CONG
 * @date 2019-10-09 18:36
 **/

@Mapper
public interface SchoolMapper {

    /**
     * 获取学校
     * @param schoolId
     * @return
     */
    School getSchoolById(Integer schoolId);


}
