package com.are_you_learn.dao;

import com.are_you_learn.beans.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author CONG
 * @date 2019-09-29 15:38
 **/
@Mapper

public interface TeacherMapper {


    /**
     * 添加老师
     * @param teacher
     */
    void insertTeacher(Teacher teacher);

    /**
     * 查找老师
     * @param teacherUsername
     */
    int findTeacher(String teacherUsername);


    /**
     * 按username查找老师
     * @param teacherUsername
     * @return
     */
    Teacher selectTeacherByUsername(String teacherUsername);


    /**
     * 添加登陆记录
     * @param record
     */
    void addLoginRecord(Map<String, Object> record);
}
