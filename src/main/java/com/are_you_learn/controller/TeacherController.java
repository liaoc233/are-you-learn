package com.are_you_learn.controller;

import com.are_you_learn.beans.Msg;
import com.are_you_learn.beans.Student;
import com.are_you_learn.beans.Teacher;
import com.are_you_learn.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.are_you_learn.Tools.Validation.isChinese;
import static com.are_you_learn.Tools.Validation.isEmail;

/**
 * @author CONG
 * @date 2019-09-30 17:10
 **/

@RequestMapping("/are_you_learn")
@CrossOrigin
@Controller
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @ResponseBody
    @RequestMapping(value = "/teacherRegist",method = RequestMethod.POST)
    public Msg teacherRegist(String realname,String password,String username,String email){


        Teacher teacher = new Teacher(null,realname,password,username,null,null,null,email,null);

        teacherService.saveTeacher(teacher);

        return Msg.success();
    }

    /**
     * 教师登陆
     * @param username,password
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/teacherLogin",method = RequestMethod.POST)
    public Msg studentLogin(@RequestParam("username") String username, @RequestParam("password") String password,HttpServletResponse response){

        Teacher teacher = teacherService.findTeacherByUsername(username);
        if(teacher==null)
            return Msg.fail().add("msg","用户不存在");

        if(teacher.getTeacherPassword().equals(password)){

            //获取登陆时间 添加登陆记录
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = df.format(new Date());
            Map<String,Object> record = new HashMap<>();
            record.put("username",teacher.getTeacherUsername());
            record.put("role","teacher");
            record.put("logintime",currentTime);
            teacherService.addLoginRecord(record);

            Cookie cookie1 = new Cookie("username",username);
            Cookie cookie2 = new Cookie("role","teacher");
            cookie1.setPath("/");
            cookie2.setPath("/");
            cookie1.setDomain("localhost");
            cookie2.setDomain("localhost");
            response.addCookie(cookie1);
            response.addCookie(cookie2);

            return Msg.success();

        }else {
            return Msg.fail().add("msg","密码错误");
        }
    }

    /**
     * 工号查重
     * @param username
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findTeacher",method = RequestMethod.POST)
    public Msg findTeacher(@RequestParam(value = "username") String username){

        boolean flag = teacherService.findTeacher(username);

        return Msg.success().add("flag",flag);
    }

}
