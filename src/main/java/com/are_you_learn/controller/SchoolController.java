package com.are_you_learn.controller;

import com.are_you_learn.beans.Msg;
import com.are_you_learn.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author CONG
 * @date 2019-10-09 18:32
 **/

@Controller
@RequestMapping("/are_you_learn")
public class SchoolController {

    @Autowired
    SchoolService schoolService;

    /**
     * 获取学校名称
     * @param schoolId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSchoolName",method = RequestMethod.POST)
    public Msg getSchoolName(@RequestParam("schoolId")Integer schoolId){

        String schoolName = schoolService.getSchoolNameById(schoolId);

        return Msg.success().add("schoolName",schoolName);

    }
}
