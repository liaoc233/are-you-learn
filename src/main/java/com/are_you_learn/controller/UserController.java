package com.are_you_learn.controller;

import com.are_you_learn.beans.Msg;
import com.are_you_learn.beans.Student;
import com.are_you_learn.beans.Teacher;
import com.are_you_learn.service.StudentService;
import com.are_you_learn.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author CONG
 * @date 2019-10-07 22:52
 **/

@RequestMapping("/are_you_learn")

@Controller
public class UserController {

    @Autowired
    StudentService studentService;

    @Autowired
    TeacherService teacherService;

    @ResponseBody
    @RequestMapping(value = "/keepLogin",method = RequestMethod.POST)
    public Msg keepLogin(@RequestParam("username") String username,@RequestParam("role")String role){

        if(role.equals("student")){
            Student student = studentService.findStudentByUsername(username);
            return Msg.success().add("user",student);
        }else{
            Teacher teacher = teacherService.findTeacherByUsername(username);
            return Msg.success().add("user",teacher);
        }

    }
}
