package com.are_you_learn.controller;

import com.are_you_learn.beans.Msg;
import com.are_you_learn.service.AcademicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author CONG
 * @date 2019-10-09 19:48
 **/

@Controller
@RequestMapping("/are_you_learn")
public class AcademicController {


    @Autowired
    AcademicService academicService;

    /**
     * 获取专业名称
     * @param academicId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAcademicName",method = RequestMethod.POST)
    public Msg getAcademicName(@RequestParam("academicId") Integer academicId){

        String academicName =   academicService.getAcademicNameById(academicId);
        return Msg.success().add("academicName",academicName);
    }

}
