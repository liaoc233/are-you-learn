package com.are_you_learn.service;

import com.are_you_learn.beans.Academic;
import com.are_you_learn.dao.AcademicMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CONG
 * @date 2019-10-09 19:52
 **/

@Service
public class AcademicService {

    @Autowired
    AcademicMapper academicMapper;

    /**
     * 获取专业名称
     * @param academicId
     * @return
     */
    public String getAcademicNameById(Integer academicId) {

        Academic academic = academicMapper.getAcademicById(academicId);

        return academic.getAcademicName();
    }
}
