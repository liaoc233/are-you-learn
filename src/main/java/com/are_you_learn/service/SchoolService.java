package com.are_you_learn.service;

import com.are_you_learn.beans.School;
import com.are_you_learn.dao.SchoolMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CONG
 * @date 2019-10-09 18:36
 **/

@Service
public class SchoolService {


    @Autowired
    SchoolMapper schoolMapper;


    /**
     * 获取学校名称
     * @param schoolId
     * @return
     */
    public String getSchoolNameById(Integer schoolId) {

        School school = schoolMapper.getSchoolById(schoolId);

        return school.getSchoolName();
    }
}
