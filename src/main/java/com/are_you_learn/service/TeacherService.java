package com.are_you_learn.service;

import com.are_you_learn.beans.Teacher;
import com.are_you_learn.dao.TeacherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author CONG
 * @date 2019-09-30 17:12
 **/

@Service
public class TeacherService {

    @Autowired
    TeacherMapper teacherMapper;


    /**
     * 添加老师
     * @param teacher
     */
    public void saveTeacher(Teacher teacher){

        teacherMapper.insertTeacher(teacher);

    }
    /**
     * 查找该工号是否存在
     * @param teacherUsername
     */
    public boolean findTeacher(String teacherUsername){

        int num = teacherMapper.findTeacher(teacherUsername);

        if(num == 0){
            return false;    //不存在
        }else {
            return true;   //存在
        }
    }

    /**
     * 按username查找老师
     * @param teacherUsername
     * @return
     */
    public Teacher findTeacherByUsername(String teacherUsername) {
        return teacherMapper.selectTeacherByUsername(teacherUsername);
    }

    public void addLoginRecord(Map<String, Object> record) {
        teacherMapper.addLoginRecord(record);
    }
}
