package com.are_you_learn.service;

import com.are_you_learn.beans.Student;
import com.are_you_learn.dao.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sound.midi.Soundbank;
import java.util.List;
import java.util.Map;

/**
 * @author CONG
 * @date 2019-09-22 20:22
 **/

@Service
public class StudentService {

    @Autowired
    StudentMapper studentMapper;


    /**
     * 添加学生
     * @param student
     */
    public void saveStudent(Student student){
        studentMapper.insertStudent(student);
    }

    /**
     * 查找该学号是否存在
     * @param studentUsername
     * @return
     */
    public boolean findStudent(String studentUsername){

        int num = studentMapper.findStudent(studentUsername);

        if(num == 0){
            return false;    //不存在
        }else {
            return true;   //存在
        }
    }

    /**
     * 按username查找学生
     * @param studentUsername
     * @return
     */
    public Student findStudentByUsername(String studentUsername) {
        return studentMapper.selectStudentByUsername(studentUsername);
    }

    public void addLoginRecord(Map<String, Object> record) {
        studentMapper.addLoginRecord(record);
    }
}
