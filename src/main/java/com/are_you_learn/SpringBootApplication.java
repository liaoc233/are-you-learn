package com.are_you_learn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author CONG
 * @date 2019-09-22 19:53
 **/

@MapperScan("com.are_you_learn.dao")
@org.springframework.boot.autoconfigure.SpringBootApplication
public class SpringBootApplication  extends SpringBootServletInitializer {

    public static void main(String[] args) {

        //spring应用启动起来
        SpringApplication.run(SpringBootApplication.class,args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SpringBootApplication.class);
    }
}

