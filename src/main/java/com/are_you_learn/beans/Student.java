package com.are_you_learn.beans;

/**
 * @author CONG
 * @date 2019-09-21 9:15
 **/
public class Student {
    private Integer studentId;
    private String studentRealname;
    private String studentUsername;
    private String studentPassword;
    private Integer studentGender;
    private Integer studentSchool;
    private Integer studentAcademic;
    private String studentPhoto;
    private String studentEmail;
    private String studentIntro;

    public Student() {
    }

    public Student(Integer studentId, String studentRealname, String studentUsername, String studentPassword, Integer studentGender, Integer studentSchool, Integer studentAcademic, String studentPhoto, String studentEmail, String studentIntro) {
        this.studentId = studentId;
        this.studentRealname = studentRealname;
        this.studentUsername = studentUsername;
        this.studentPassword = studentPassword;
        this.studentGender = studentGender;
        this.studentSchool = studentSchool;
        this.studentAcademic = studentAcademic;
        this.studentPhoto = studentPhoto;
        this.studentEmail = studentEmail;
        this.studentIntro = studentIntro;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentRealname() {
        return studentRealname;
    }

    public void setStudentRealname(String studentRealname) {
        this.studentRealname = studentRealname;
    }

    public String getStudentUsername() {
        return studentUsername;
    }

    public void setStudentUsername(String studentUsername) {
        this.studentUsername = studentUsername;
    }

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }

    public Integer getStudentGender() {
        return studentGender;
    }

    public void setStudentGender(Integer studentGender) {
        this.studentGender = studentGender;
    }

    public Integer getStudentSchool() {
        return studentSchool;
    }

    public void setStudentSchool(Integer studentSchool) {
        this.studentSchool = studentSchool;
    }

    public Integer getStudentAcademic() {
        return studentAcademic;
    }

    public void setStudentAcademic(Integer studentAcademic) {
        this.studentAcademic = studentAcademic;
    }

    public String getStudentPhoto() {
        return studentPhoto;
    }

    public void setStudentPhoto(String studentPhoto) {
        this.studentPhoto = studentPhoto;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentIntro() {
        return studentIntro;
    }

    public void setStudentIntro(String studentIntro) {
        this.studentIntro = studentIntro;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", studentRealname='" + studentRealname + '\'' +
                ", studentUsername='" + studentUsername + '\'' +
                ", studentPassword='" + studentPassword + '\'' +
                ", studentGender=" + studentGender +
                ", studentSchool=" + studentSchool +
                ", studentAcademic=" + studentAcademic +
                ", studentPhoto='" + studentPhoto + '\'' +
                ", studentEmail='" + studentEmail + '\'' +
                ", studentIntro='" + studentIntro + '\'' +
                '}';
    }
}
