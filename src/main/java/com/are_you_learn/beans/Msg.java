package com.are_you_learn.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CONG
 * @date 2019-09-29 15:26
 **/
public class Msg {
    //  状态码
    private int code;
    //  提示信息
    private String msg;

    //  用户要返回给浏览器的信息
    private Map<String,Object> extend = new HashMap<String, Object>();

    public Msg() {
        super();
    }
    public Msg(int code, String msg, Map<String, Object> extend) {
        this.code = code;
        this.msg = msg;
        this.extend = extend;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getExtend() {
        return extend;
    }

    public void setExtend(Map<String, Object> extend) {
        this.extend = extend;
    }

    //成功
    public static Msg success(){
        Msg result = new Msg();
        result.setCode(200);
        result.setMsg("处理成功！");
        return result;
    }

    //失败
    public static Msg fail(){
        Msg result = new Msg();
        result.setCode(400);
        result.setMsg("处理失败！");
        return result;
    }

    //添加数据
    public Msg add(String key,Object value){
        this.getExtend().put(key,value);
        return this;
    }
}
