package com.are_you_learn.beans;

/**
 * @author CONG
 * @date 2019-10-09 19:44
 **/
public class Academic {

    private Integer academicId;
    private String academicName;
    private Integer academicSchool;

    public Academic() {
    }

    public Academic(Integer academicId, String academicName, Integer academicSchool) {
        this.academicId = academicId;
        this.academicName = academicName;
        this.academicSchool = academicSchool;
    }

    public Integer getAcademicId() {
        return academicId;
    }

    public void setAcademicId(Integer academicId) {
        this.academicId = academicId;
    }

    public String getAcademicName() {
        return academicName;
    }

    public void setAcademicName(String academicName) {
        this.academicName = academicName;
    }

    public Integer getAcademicSchool() {
        return academicSchool;
    }

    public void setAcademicSchool(Integer academicSchool) {
        this.academicSchool = academicSchool;
    }

    @Override
    public String toString() {
        return "Academic{" +
                "academicId=" + academicId +
                ", academicName='" + academicName + '\'' +
                ", academicSchool=" + academicSchool +
                '}';
    }
}
