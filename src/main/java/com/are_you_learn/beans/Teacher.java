package com.are_you_learn.beans;

/**
 * @author CONG
 * @date 2019-09-22 16:38
 **/
public class Teacher {

    private Integer teacherId;
    private String teacherRealname;
    private String teacherPassword;
    private String teacherUsername;
    private Integer teacherGender;
    private Integer teacherSchool;
    private String teacherPhoto;
    private String teacherEmail;
    private String teacherIntro;

    public  Teacher(){

    }

    public Teacher(Integer teacherId, String teacherRealname, String teacherPassword, String teacherUsername, Integer teacherGender, Integer teacherSchool, String teacherPhoto, String teacherEmail, String teacherIntro) {
        this.teacherId = teacherId;
        this.teacherRealname = teacherRealname;
        this.teacherPassword = teacherPassword;
        this.teacherUsername = teacherUsername;
        this.teacherGender = teacherGender;
        this.teacherSchool = teacherSchool;
        this.teacherPhoto = teacherPhoto;
        this.teacherEmail = teacherEmail;
        this.teacherIntro = teacherIntro;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherRealname() {
        return teacherRealname;
    }

    public void setTeacherRealname(String teacherRealname) {
        this.teacherRealname = teacherRealname;
    }

    public String getTeacherPassword() {
        return teacherPassword;
    }

    public void setTeacherPassword(String teacherPassword) {
        this.teacherPassword = teacherPassword;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public Integer getTeacherGender() {
        return teacherGender;
    }

    public void setTeacherGender(Integer teacherGender) {
        this.teacherGender = teacherGender;
    }

    public Integer getTeacherSchool() {
        return teacherSchool;
    }

    public void setTeacherSchool(Integer teacherSchool) {
        this.teacherSchool = teacherSchool;
    }

    public String getTeacherPhoto() {
        return teacherPhoto;
    }

    public void setTeacherPhoto(String teacherPhoto) {
        this.teacherPhoto = teacherPhoto;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherIntro() {
        return teacherIntro;
    }

    public void setTeacherIntro(String teacherIntro) {
        this.teacherIntro = teacherIntro;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherId=" + teacherId +
                ", teacherRealname='" + teacherRealname + '\'' +
                ", teacherPassword='" + teacherPassword + '\'' +
                ", teacherUsername='" + teacherUsername + '\'' +
                ", teacherGender=" + teacherGender +
                ", teacherSchool=" + teacherSchool +
                ", teacherPhoto='" + teacherPhoto + '\'' +
                ", teacherEmail='" + teacherEmail + '\'' +
                ", teacherIntro='" + teacherIntro + '\'' +
                '}';
    }
}
