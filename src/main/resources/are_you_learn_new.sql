/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.6.45 : Database - are_you_learn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`are_you_learn` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `are_you_learn`;

/*Table structure for table `stu_add_class` */

DROP TABLE IF EXISTS `stu_add_class`;

CREATE TABLE `stu_add_class` (
  `student_id` int(9) DEFAULT NULL,
  `class_id` int(9) DEFAULT NULL,
  `enter_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `stu_add_class` */

/*Table structure for table `tb_academic` */

DROP TABLE IF EXISTS `tb_academic`;

CREATE TABLE `tb_academic` (
  `academic_id` int(9) NOT NULL,
  `academic_name` varchar(16) NOT NULL,
  `academic_school` int(9) NOT NULL,
  PRIMARY KEY (`academic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_academic` */

insert  into `tb_academic`(`academic_id`,`academic_name`,`academic_school`) values (1,'计算机科学与技术',1),(2,'计算机科学与技术(教育)',1),(3,'物联网工程',1),(4,'数学',2);

/*Table structure for table `tb_classes` */

DROP TABLE IF EXISTS `tb_classes`;

CREATE TABLE `tb_classes` (
  `class_id` int(9) NOT NULL,
  `class_name` varchar(15) NOT NULL,
  `class_intro` varchar(200) DEFAULT NULL,
  `class_teacher` int(9) NOT NULL,
  `class_lock` int(1) NOT NULL,
  `clsss_photo` varchar(50) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_classes` */

/*Table structure for table `tb_school` */

DROP TABLE IF EXISTS `tb_school`;

CREATE TABLE `tb_school` (
  `school_id` int(9) NOT NULL,
  `school_name` varchar(16) NOT NULL,
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_school` */

insert  into `tb_school`(`school_id`,`school_name`) values (1,'北京大学'),(2,'清华大学');

/*Table structure for table `tb_students` */

DROP TABLE IF EXISTS `tb_students`;

CREATE TABLE `tb_students` (
  `student_id` int(9) NOT NULL AUTO_INCREMENT,
  `student_realname` varchar(10) DEFAULT NULL,
  `student_username` varchar(16) DEFAULT NULL,
  `student_gender` int(1) DEFAULT NULL,
  `student_school` int(9) DEFAULT NULL,
  `student_academic` int(9) DEFAULT NULL,
  `student_photo` varchar(100) DEFAULT NULL,
  `student_email` varchar(50) DEFAULT NULL,
  `student_intro` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tb_students` */

insert  into `tb_students`(`student_id`,`student_realname`,`student_username`,`student_gender`,`student_school`,`student_academic`,`student_photo`,`student_email`,`student_intro`) values (1,'123','123',0,1,1,'123','123@123.com','');

/*Table structure for table `tb_teachers` */

DROP TABLE IF EXISTS `tb_teachers`;

CREATE TABLE `tb_teachers` (
  `teacher_id` int(9) NOT NULL AUTO_INCREMENT,
  `teacher_realname` varchar(10) NOT NULL,
  `teacher_gender` int(1) DEFAULT NULL,
  `teacher_school` int(9) DEFAULT NULL,
  `teacher_photo` varchar(100) NOT NULL,
  `teacher_email` varchar(50) DEFAULT NULL,
  `teacher_intro` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_teachers` */

/*Table structure for table `tb_user_login` */

DROP TABLE IF EXISTS `tb_user_login`;

CREATE TABLE `tb_user_login` (
  `user_id` varchar(10) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_user_login` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
