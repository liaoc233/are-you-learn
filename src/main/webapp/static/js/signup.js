
$(document).ready(function(){


    $('#login').click(function(){

        var username=$('#username').val();
        var password=$('#password').val();
        var role= $('input:radio:checked').val();
        var url='';

        if(username==''||password==''){
            alert('不能有空值！');
            return;
        }
        if(role=="student")
            url="http://localhost:8080/are_you_learn/studentLogin";

        if(role=="teacher")
            url = "http://localhost:8080/are_you_learn/teacherLogin";

        var text = $("#user-login").serialize();

        $.ajax({
            url: url,
            type: "POST",
            xhrFields: {withCredentials: true},
            data: text,
            async: false,
            success: function (result) {
                if(result.code == 400){
                    console.log(result);
                    alert(result.extend.msg);
                }
                if(result.code == 200){
                    //跳转到主页
                    window.location.href="../../index.html";
                }

            }
        });

    });

});

